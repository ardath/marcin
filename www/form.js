$( document ).ready(function(){
  var default_domain = "PL";
  var default_term = "00";

  generateRange(default_domain, default_term);
  selectFirstOptions();

  $("#domain,#term").click(function(){
    var domain = $( "#domain option:selected" ).val();
    var term = $( "#term option:selected" ).val();

    generateRange(domain, term);
  });

  $("#show-map").click(function(event){
    event.preventDefault();
    var link = generateLink();
    console.log("link:"+link);
    alert("link:"+link);
  })

});

function selectFirstOptions(){
  formParts = $('#main-form').children();
    console.log(formParts);

  $.each(formParts, function(index,child){
    // console.log(child);
    $(child).children(":first").attr('selected','selected');
  });
}

function selectDefaultOptions(){
  $('#domain option[value=PL]').attr('selected','selected');
  $('#term option[value=00]').attr('selected','selected');
  $('#range :first-child').attr('selected','selected');
}

function generateRange(domain, term){
  if (domain == "PL") {
    time_steps = [1, 3];
    time_range = 30; // [h]
  } else {
    time_steps = [3, 6, 12, 24];
    time_range = 60; // [h]
  }
  newRange = generateRangeSelect(time_steps, time_range);
  $( "#range" ).replaceWith(newRange);
}

function generateRangeSelect(steps, range){
  var select_node = '<select id="range" name="range" size=12>';
  $.each(steps, function(index,time_step){
    var number_of_steps = range/time_step;

    for(time_index = 0; time_index < number_of_steps; time_index++){
      var time_start = time_index * time_step;
      var time_end = (time_index+1)*time_step;

      var tag_start = '<option value="'+time_step+'; '+time_index+'">';
      var text = time_step + "h: +" + time_start + "h - +" + time_end + "h";
      var tag_end = '</option>';
      select_node += tag_start+text+tag_end;
    }
  });
  select_node += "</select>";
  return select_node; 
}

function generateLink(){
  var domain = $( "#domain option:selected" ).val();
  var term = $( "#term option:selected" ).val();
  var date = $( "#date option:selected" ).val();
  var range = $( "#range option:selected" ).val();

  return [date, domain, term, range].join("; "); // + field;
}
